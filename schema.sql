
drop table if exists user_account;
drop table if exists collection;
drop table if exists flashcard;


create table if not exists user_account (
  id serial primary key
);


create table if not exists collection (
  id serial primary key,
  user_id int,
  title varchar(255) not null,
  
);

create table if not exists flashcard (
  id serial primary key,
  collection_id int references collection (id),
  title varchar(255) not null,
  content text
);
