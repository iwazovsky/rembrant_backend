const {
  Model,
} = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class Collection extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate({ Flashcard }) {
      this.hasMany(Flashcard, { as: 'flashcards', foreignKey: 'collection_id', onDelete: 'CASCADE' });
    }
  }

  const types = {
    userAccountId: {
      type: DataTypes.INTEGER,
      field: 'user_account_id',
      references: {
        model: 'user_account',
      },
    },
    title: {
      type: DataTypes.STRING,
      field: 'title'
    }, 
    // flashcards: {
      
    // }
  };

  const options = {
    sequelize,
    modelName: 'Collection',
    underscored: true,
    tableName: 'collection',
    freezeTableName: true,
  };

  Collection.init(types, options);

  return Collection;
};
