const {
  Model,
} = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class Flashcard extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate({ Collection }) {
      this.belongsTo(Collection, {
        foreignKey: 'collection_id',
      });
    }
  }
  Flashcard.init({
    collectionId: {
      type: DataTypes.INTEGER,
      field: 'collection_id',
      references: {
        model: 'collection'
      }
    },
    title: {
      type: DataTypes.STRING
    },
    content: {
      type: DataTypes.TEXT
    }
  }, {
    sequelize,
    modelName: 'Flashcard',
    underscored: true,
    tableName: 'flashcard',
    freezeTableName: true
  });
  return Flashcard;
};
