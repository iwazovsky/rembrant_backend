const {
  Model,
} = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class UserAccount extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    // static associate({ Collection }) {
    //   this.hasMany(Collection, { foreignKey: 'user_account_id' });
    // }
  }
  UserAccount.init({
    firstName: {
      type: DataTypes.STRING,
      field: 'first_name'
    },
    lastName: {
      type: DataTypes.STRING,
      field: 'last_name'
    }
  }, {
    sequelize,
    modelName: 'UserAccount',
    underscored: true,
    tableName: 'user_account',
    freezeTableName: true,
  });
  return UserAccount;
};
