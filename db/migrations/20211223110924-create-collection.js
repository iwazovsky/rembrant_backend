/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('collection', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      user_account_id: {
        type: Sequelize.INTEGER,
        references: {
          model: 'user_account',
          key: 'id',
        },
      },
      title: {
        allowNull: false,
        type: Sequelize.STRING,
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    },
    {
      indexes: [
        {
          unique: true,
          fields: ['user_account_id', 'title']
        }
      ]
    }
    );
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('collection');
  },
};
