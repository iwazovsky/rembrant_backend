/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    //
    //  * Add seed commands here.
    //  *
    //  * Example:*/
    await queryInterface.bulkInsert('user_account', [{
      id: 1,
      first_name: 'Kostya',
      last_name: 'Rembrant',
      created_at: new Date(),
      updated_at: new Date(),
    }], {});
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example: */
    await queryInterface.bulkDelete('user_account', null, {});
  },
};
