'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
    return queryInterface.bulkInsert('collection', [
      {
        user_account_id: 1,
        title: "English words",
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        user_account_id: 1,
        title: "Билеты к экзамену по английскому (IES2023/12)",
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        user_account_id: 1,
        title: "iOS abbreviations and terms",
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        user_account_id: 1,
        title: "European capitals",
        created_at: new Date(),
        updated_at: new Date()
      },
    ]);
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete('Collection', null, {});
  }
};
