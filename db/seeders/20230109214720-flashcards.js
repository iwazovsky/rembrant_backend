'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
   return queryInterface.bulkInsert('flashcard', [
      {
        collection_id: 1,
        title: "Sky",
        content: "Небо",
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        collection_id: 1,
        title: "Tree",
        content: "Дерево",
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        collection_id: 1,
        title: "Earth",
        content: "Земля",
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        collection_id: 1,
        title: "Fire",
        content: "Огонь",
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        collection_id: 1,
        title: "Wind",
        content: "Ветер",
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        collection_id: 1,
        title: "Box",
        content: "Коробка",
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        collection_id: 1,
        title: "Car",
        content: "Машина",
        created_at: new Date(),
        updated_at: new Date()
      },

      
      {
        collection_id: 2,
        title: "Car",
        content: "Машина",
        created_at: new Date(),
        updated_at: new Date()
      },


      {
        collection_id: 3,
        title: "CA",
        content: "Core Animations",
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        collection_id: 3,
        title: "CG",
        content: "Core Graphics",
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        collection_id: 3,
        title: "CA",
        content: "Core Animations",
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        collection_id: 3,
        title: "ARC",
        content: "Automatic Reference Counting",
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        collection_id: 3,
        title: "Deadlock",
        content: "Concurrent threads blocking each other",
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        collection_id: 3,
        title: "NS",
        content: "NextStep",
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        collection_id: 3,
        title: "DRY",
        content: "Don't repeat yourself",
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        collection_id: 3,
        title: "KISS",
        content: "Keep it simple, stupid",
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        collection_id: 3,
        title: "SOLID",
        content: "Single Responsibility, Open/Close principle, Liskov substitution, Interface segregation, Dependency inversion",
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        collection_id: 3,
        title: "VIPER",
        content: "View Interactor Presenter Entity Router",
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        collection_id: 3,
        title: "MVVM",
        content: "Model View ViewModel",
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        collection_id: 3,
        title: "MVC",
        content: "Model View Controller",
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        collection_id: 3,
        title: "MVP",
        content: "Model View Presenter",
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        collection_id: 3,
        title: "MVI",
        content: "Model View Interactor",
        created_at: new Date(),
        updated_at: new Date()
      },

      {
        collection_id: 4,
        title: "Latvia",
        content: "Riga",
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        collection_id: 4,
        title: "Estonia",
        content: "Tallinn",
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        collection_id: 4,
        title: "Finland",
        content: "Helsinki",
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        collection_id: 4,
        title: "Sweden",
        content: "Stockholm",
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        collection_id: 4,
        title: "Norway",
        content: "Oslo",
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        collection_id: 4,
        title: "Iceland",
        content: "Reykjavik",
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        collection_id: 4,
        title: "Denmark",
        content: "Copenhagen",
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        collection_id: 4,
        title: "Germany",
        content: "Berlin",
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        collection_id: 4,
        title: "Portugal",
        content: "Lisbon",
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        collection_id: 4,
        title: "Spain",
        content: "Madrid",
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        collection_id: 4,
        title: "Russia",
        content: "Moscow",
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        collection_id: 4,
        title: "Ukraine",
        content: "Kiev",
        created_at: new Date(),
        updated_at: new Date()
      },
      {
        collection_id: 4,
        title: "Belorussia",
        content: "Minsk",
        created_at: new Date(),
        updated_at: new Date()
      },
    ]);
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
