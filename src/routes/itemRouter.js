const itemRouter = require('express').Router({ mergeParams: true });
const {
  getFlashcardsList, createFlashcard, getFlashcard, editFlashcard, deleteFlashcard,
} = require('../controllers/flashcardController');

itemRouter.route('/')
  .post(createFlashcard)
  .get(getFlashcardsList);

itemRouter.route('/:flashcardId')
  .get(getFlashcard)
  .put(editFlashcard)
  .delete(deleteFlashcard);

module.exports = itemRouter;
