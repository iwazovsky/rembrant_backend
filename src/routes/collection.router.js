const router = require('express').Router();
const itemRouter = require('./itemRouter');
const {
  getCollectionsList, createCollection, getCollection, deleteCollection, editCollection,
} = require('../controllers/collectionController');

router.route('/')
  .post(createCollection)
  .get(getCollectionsList);

router.route('/:collectionId')
  .get(getCollection)
  .put(editCollection)
  .delete(deleteCollection);

router.use('/:collectionId/flashcards', itemRouter);
module.exports = router;
