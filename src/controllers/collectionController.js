const { Collection, Flashcard, sequelize } = require('../../db/models');
const session = require('../session.json');
const Sequelize = require('sequelize');

const createCollection = async (req, res) => {
  const { title } = req.body;

  try {
    const [newCollection, created] = await Collection.findOrCreate({
      include: [{
        model: Flashcard,
        as: 'flashcards',
        required: false
      }],
      where: { 
        title 
      },
      defaults: {
        userAccountId: Number(session.id),
      },
    });

    if (!created) {
      throw new Error('Collection is not created');
    }

    const collection = await Collection.findOne({ 
      include: [{
        model: Flashcard,
        as: 'flashcards',
        required: false
      }],
      where: { 
        id: Number(newCollection.id) } 
      }
    );

    res.status(201)
      .json(collection);
  } catch (error) {
    console.error(error);
    res.sendStatus(400);
  }
};



const getCollectionsList = async (req, res) => {
  try {
    const collectionsList = await Collection.findAll({
      include: [{
        model: Flashcard,
        as: 'flashcards',
        required: false
      }],
      where: {
        userAccountId: Number(session.id), // тут надо будет по-другому написать потом
      },
      order: [
        ['createdAt', 'DESC']
      ]
    });
    // for (collection in collectionsList) {
    //   const [result, metadata] = await sequelize.query("SELECT count(*) as total FROM flashcard WHERE collection_id = " + collectionsList[collection].id);
    //   collectionsList[collection]['dataValues']['flashcardsTotal'] = parseInt(metadata.rows[0].total);
    // }

    res.json(collectionsList);
  } catch (error) {
    console.log(error);
    res.sendStatus(500);
  }
};

const getCollection = async (req, res) => {
  const { collectionId } = req.params;
  try {
    const collectionToGet = await Collection.findOne({ 
      include: [{
        model: Flashcard,
        as: 'flashcards',
        required: false
      }],
      where: { 
        id: Number(collectionId) } 
      }
    );
    if (collectionToGet) {
      res.json(collectionToGet);
    } else {
      res.sendStatus(404);
    }
  } catch (error) {
    res.sendStatus(400);
  }
};

const editCollection = async (req, res) => {
  const { collectionId } = req.params;
  const { title } = req.body;

  try {
    const collectionToUpdate = await Collection.update(
      { title },
      { where: { id: Number(collectionId) } },
    );

    if (collectionToUpdate) {
      const updatedCollection = await Collection.findOne({ 
        include: [{
          model: Flashcard,
          as: 'flashcards',
          required: false
        }],
        where: { 
          id: Number(collectionId) 
        } 
      });
      res.status(200).json(updatedCollection);
    } else {
      res.sendStatus(404);
    }
  } catch (error) {
    res.sendStatus(400);
  }
};

const deleteCollection = async (req, res) => {
  const { collectionId } = req.params;
  try {
    const collectionToDelete = await Collection.destroy({
      where: { 
        id: Number(collectionId) 
      },
    });
    if (collectionToDelete) {
      res.sendStatus(204);
    } else {
      res.sendStatus(404);
    }
  } catch (error) {
    console.log(error);
    res.sendStatus(400);
  }
};

module.exports = {
  createCollection, getCollectionsList, editCollection, deleteCollection, getCollection,
};
