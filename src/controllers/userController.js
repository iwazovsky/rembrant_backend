const { User } = require('../../db/models');

const getUser = async (req, res) => {
  const { id } = req.params;
  try {
    const currentUser = await User.findOne({ where: { id: Number(id) } });
    res.json(currentUser);
  } catch (error) {
    console.log(error);
    res.sendStatus(500);
  }
};

module.exports = { getUser };
