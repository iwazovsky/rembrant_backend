const { Flashcard } = require('../../db/models');

const createFlashcard = async (req, res) => {
  const { title, content } = req.body;
  const { collectionId } = req.params;
  try {
    const [newFlashcard, created] = await Flashcard.findOrCreate({
      where: { title },
      defaults: {
        collectionId: Number(collectionId),
        content,
      },
    });
    if (created) {
      res.status(201).json(newFlashcard);
    } else {
      res.sendStatus(409);
    }
  } catch (error) {
    console.log(error);
    res.sendStatus(400);
  }
};

const getFlashcardsList = async (req, res) => {
  const { collectionId } = req.params;
  try {
    const itemsList = await Flashcard.findAll({
      where: {
        collectionId: Number(collectionId),
      },
      order: [
        ['createdAt', 'DESC']
      ]
    });
    res.json(itemsList);
  } catch (error) {
    console.error(error);
    res.sendStatus(500);
  }
};
const getFlashcard = async (req, res) => {
  const { flashcardId } = req.params;
  try {
    const itemToGet = await Flashcard.findOne(
      { where: { id: Number(flashcardId) } },
    );
    if (itemToGet) {
      res.json(itemToGet);
    } else {
      res.sendStatus(404);
    }
  } catch (error) {
    res.sendStatus(400);
  }
};

const editFlashcard = async (req, res) => {
  const { flashcardId } = req.params;
  const { title, content } = req.body;
  try {
    const itemToUpdate = await Flashcard.update(
      { title, content },
      { where: { id: Number(flashcardId) } },
    );
    if (itemToUpdate) {
      const updatedFlashcard = await Flashcard.findOne(
        { where: { id: Number(flashcardId) } },
      );
      res.status(200).json(updatedFlashcard);
    } else {
      res.sendStatus(404);
    }
  } catch (error) {
    console.log(error);
    res.sendStatus(400);
  }
};

const deleteFlashcard = async (req, res) => {
  const { flashcardId } = req.params;
  try {
    const itemToDelete = await Flashcard.destroy({
      where: { id: Number(flashcardId) },
    });
    if (itemToDelete) {
      res.sendStatus(204);
    } else {
      res.sendStatus(404);
    }
  } catch (error) {
    res.sendStatus(400);
  }
};

module.exports = {
  getFlashcardsList, createFlashcard, getFlashcard, editFlashcard, deleteFlashcard,
};
